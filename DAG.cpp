#include <stdio.h>
#include <stdlib.h>
#include <time.h>
#include"main.h"
#include <iostream>

extern double Output[OUTPUT_SIZE];

int x,y;
///Bipartite graph maximum matching http://judge.u-aizu.ac.jp/onlinejudge/review.jsp?rid=2120131#1
bool D[N+10][N+10] = {};
int PY[N+10];
bool V_[N+10];

bool match(int x) {
  if(x < 0) return true;
  if(V_[x]) return false;
  V_[x] = true;
  for(int y = 0; y < N; ++y) {
    if(!D[x][y]) continue;
    if(match(PY[y])) {
      PY[y] = x;
      return true;
    }
  }
  return false;
}
///////////////////////////////////


int G_(int *G,int i,int j){
	return G[i*N + j];
}

int checkDAG(int n,int *G,int j,int *k){
	if(*k!=0) return 1;
	if(n==0) return 1;

	for(int i=0;i<N;i++){
		if(G_(G,j,i))	{
			*k = *k + checkDAG(n-1,G,i,k);
		}
	}
	return 0;
}

void showDAG(int *G){
	for(int i=0;i<N;i++){
		for(int j=0;j<N;j++){
			printf("%d ",G[i*N+j]);
		}
		printf("\n");
	}

}

void copyGrow(int *G,int a,int b){	
	for(int i=b+1;i<N;i++){
		G[(N*a)+i]=(G[(N*a)+i]|G[(N*b)+i]);
	}
}

void DAG(int *G){	
	if(SHOW_DAG)	showDAG(G);
	/* calculate width of DAG */
	for(int i=0;i<N;i++){
		for(int j=i+1;j<N;j++){
			if(G[(i*N)+j]==1){
				D[i][j] = true;
			}else D[i][j] = false;
		}
	}
	fill(PY,PY+N,-1);
	int count = 0;
	for(int x = 0; x < N; ++x) {
		fill(V_,V_+N,false);
		if(match(x)) ++count;
	}

	/* output width of DAG */
	Output[1] = ((double) N-(double) count);
	
	/* output DAG */
	FILE *fp;	
	if ((fp = fopen("dag.cvs", "w")) == NULL) {
		printf("file open error!!\n");
	}
	for(int i=0;i<N;i++){
		for(int j=0;j<N;j++){
			fprintf(fp,"%d ",G[i*N+j]);
		}
		fprintf(fp,"\n");
	}
	fclose(fp);
}

