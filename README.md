PiMDD: data structure for job schejuling with precedence constraints.  
This repository is for the paper submitted to SEA2018 and this file is still incomplete.  
====

##Contents
* main.cpp: main program
* DAG.cpp: sub-program for input
* PiMDD.cpp: proposed method
* PermutationMatrix.cpp: existing method using perumation matrices
* ComparisonMatrix.cpp: existing method using comparison matrices 
* main.h : setting parameters and options
* DAG.h : header for main.cpp
* PiMDD.h : header for main.cpp
* PermutationMatrix.h: header for main.cpp
* ComparisonMatrix.h: header for main.cpp
* MT.h: pseudo-random number generator <http://www.math.sci.hiroshima-u.ac.jp/~m-mat/MT/MT2002/emt19937ar.html>
* Makefile
* demo1.gif : example usage 1
* demo2.gif : example usage 2


##Usage
NOTE: gurobi optimizer needs to be installed in your environment.For details, see <http://www.gurobi.com/documentation/>.  
1. Set parameters in main.h (say, N,PROB,and T)  
2. Edit Makefile appropriately and run make (running "make clearn" is reccomended beforehand)  
3. Execute pimdd  
4. The file "ans[N]_[T]_[PROB].csv" is created  

##Example Usage 1 demo1.gif
For N=15 jobs, generate precedence constraints by randomly constructing edges in the DAG G with nodes in {1,...,N=15} with probability PROB=0.5 for each pair of nodes, and 
solve the job scheduling problem by the three algorithms (repeatedly for T=30 times)
	1, Edit Makefile appropriately Makefile 
	2, Edit main.h so that N=15,PROB=0.5,T=30
	3, make clean
	4, make 
	5, ./pimdd
	6, The file "ans15_30_0.50.csv" is obtained



