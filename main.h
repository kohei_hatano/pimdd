//Please edit parameters.
#define N 25	// Permutation size
#define PROB 0.3	// Parameters related to DAG
#define T 1000 // Number of trials
#define SKIP_PERM 0 //whether skip PermutationMatrix or not 

//If you want to see some information, please edit these.
#define SHOW_W 1	// Show weight vector
#define SHOW_PiMDD 0	// Show PiMDD
#define SHOW_DAG 1	// Show DAG
#define SHOW_V2 0	// Show information on the directed edge of each node in PiMDD
#define OUTPUT_PiMDD 0	// Output PiMDD in graphviz format
#define SHOW_EDGE_UPDATE 1	// Show dynamic reserve size of E

//If you want to see the answer of each way, please edit these.
#define SHOW_MINPATH 0	// Show shortest path of PiMDD
#define SHOW_PiMDD_ANS 1// Show permutations in PiMDD
#define SHOW_PiMDD_W 1	// Show value in PiMDD
#define SHOW_PERM_ANS 1	// Show permutations in PermutationMatrix
#define SHOW_PERM_W 1	// Show value in PermutationMatrix
#define SHOW_COMP_ANS 1	// Show permutations in ComparisonMatrix
#define SHOW_COMP_W 1	// Show value in ComparisonMatrix

//If you want to see the time of each way, please edit these.
#define SHOW_PiMDD_PRE_TIME 1 // Show preprocessing time of PiMDD
#define SHOW_PiMDD_OPT_TIME 1 // Show optimization time of PiMDD
#define SHOW_PERM_PRE_TIME 1 // Show preprocessing time of PermutationMatrix
#define SHOW_PERM_OPT_TIME 1 // Show optimization time of PermutationMatrix
#define SHOW_COMP_PRE_TIME 1 // Show preprocessing time of ComparisonMatrix
#define SHOW_COMP_OPT_TIME 1 // Show optimization time of ComparisonMatrix


#define N4 (N/32)+1 //Required number of bytes to hold the node information
#define epsilon__ 0.00001
#define OUTPUT_SIZE 16	// The size of OUTPUT[]
#define CHECK_ANS 1 // Compare answers
//#define SEED (unsigned)time(NULL)
#define SEED 2017
#define EDGE 10000	// Initial size of edge set

using namespace std;

class node_label{
public:
	node_label(int a , unsigned int *b)
		:Number(a)
	{
		for(int i = 0; i < N4; i++)
            Label[i] = b[i];
	}
	~node_label(){}; 
	bool operator<( const node_label &right )const{
		for(int i = 0; i < N4; i++)
			if (Label[i] != right.Label[i]) {
				return Label[i] < right.Label[i];
			}
		return Label[N4-1] < right.Label[N4-1];
	}
	int Number;
	unsigned int Label[N4];
};

