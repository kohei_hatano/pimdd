#include <stdio.h>
#include <time.h>
#include <iostream>
#include <vector>
#include <set>
#include<math.h>
#include "ComparisonMatrix.h"
#include "DAG.h"
#include "PermutationMatrix.h"
#include "PiMDD.h"
#include "main.h"
#include "MT.h"

double Output[OUTPUT_SIZE];
/* 
	Output[0] : size of permutation
	Output[1] : width of DAG
	Output[2] : preprocessing time of PiMDD based method
	Output[3] : optimization time of PiMDD based method
	Output[4] : running time of PiMDD based method
	Output[5] : preprocessing time of permutation matrix based method
	Output[6] : optimization time of permutation matrix based method
	Output[7] : running time of permutation matrix based method
	Output[8] : preprocessing time of comparison matrix based method
	Output[9] : optimization time of comparison matrix based method
	Output[10] : running time of comparison matrix based method
	Output[11] : the number of PiMDD node 
	Output[12] : the number of PiMDD edge 
	Output[13] : the number of constraint which permutation matrix based method got
	Output[14] : the number of constraint which comparison matrix based method got
	Output[15] : PROB 

	V : PiMDD node set  
	v \in V : v = ( node number, node label )
	
	E : PiMDD edge set
	e \in E : e = ( node number at the root of the edge, 
					node number at the end of the edge, 
					a difference node between the node label corresponding to the node number at the root of the edge and the node label corresponding to the node number at the end of the edge, 
					size of node label corresponding to the node number at the root of the edge
				   )
	W[N]							: Weight vector
	pimdd_answer[N]					: anser of PiMDD based method
	pimdd_w							: Inner product value of PiMDD based method
	permutation_matrix_answer[N]	: anser of permutation matrix based method
	permutation_matrix_w			: Inner product value of permutation matrix based method
	comparison_matrix_answer[N]		: anser of comparison matrix based method
	comparison_matrix_w				: Inner product value of comparison matrix based method
	
*/
char filename[256];
double W[N];
int pimdd_answer[N];
double pimdd_w;	
int permutation_matrix_answer[N];	
double permutation_matrix_w;
int comparison_matrix_answer[N];
double comparison_matrix_w;
set<node_label> V;	
vector<int> E( 4 * EDGE ); 

int main(){
	/* Initialization */
	FILE *fp;
	sprintf(filename,"ans%d_%d_%.2f.cvs",N,T,PROB);
	if ((fp = fopen(filename, "w")) == NULL) {
		printf("file open error!!\n");
		return -1;
	}
	fclose(fp);
	init_genrand(SEED);
	int G[N*N] ={};//Representing DAG in Upper triangular adjacency matrix.

	/* run algorithm for T times */
	for(int i_=0;i_<T;i_++){
		/* Initialization */
		for(int i=0;i<N;i++)	W[i]=genrand_real1();
		if(SHOW_W){
			cout <<"W=" ;
			for(int i=0;i<N;i++ ) printf("%f ",W[i]);
			printf("\n");
		}
		fill( E.begin(), E.end(), 0 );
		V.clear ();

		/* make DAG */
		for(int i=0;i<N;i++){
			for(int j=i+1;j<N;j++){
					if(genrand_real1()<=(double)PROB)	G[i*N+j]=1;
					else G[i*N+j]=0;
			}
		}
		
		/* output DAG */
		DAG(G);
		
		/* try for each algorithm */
		PiMDD();
		if(!SKIP_PERM)PermutationMatrix();
		ComparisonMatrix();
		
		/* output results */
		Output[0] = (double)N;
		Output[4] = Output[2]+Output[3];
		Output[7] = Output[5]+Output[6];
		Output[15] = PROB;
		if ((fp = fopen(filename, "a")) == NULL) {
			printf("file open error!!\n");
		return -1;
		}

		for(int i=0;i<OUTPUT_SIZE;i++){
			fprintf(fp,"%g\t",Output[i]);	
		}
		fprintf(fp,"\n");
		fclose(fp);

		if(CHECK_ANS){
		  if(!SKIP_PERM){
		    if((fabs(pimdd_w - permutation_matrix_w) >= epsilon__)||(fabs(comparison_matrix_w - permutation_matrix_w) >= epsilon__)){
				printf("Different answers ");
				return -1;
		    }
		  }
		  else 
		    if((fabs(pimdd_w -comparison_matrix_w ) >= epsilon__)){
		      printf("Different answers %d",i_);
		      return -1;}
		}

	}
	return 0;
}
