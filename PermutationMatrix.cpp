#include <stdio.h>
#include <time.h> 
#include "main.h"
#include "gurobi_c++.h"


extern double Output[OUTPUT_SIZE];
extern double W[N];
extern int permutation_matrix_answer[N];
extern double permutation_matrix_w;

int PermutationMatrix(){
	clock_t start = clock();    // preprocessing time
	/* Input DAG */
	FILE *fp;
	int G[N][N];
	if ((fp = fopen("dag.cvs", "r")) == NULL) {
		printf("prem file open error!!\n");
		return -1;
	}
	for(int i=0;i<N;i++){
		for(int j=0;j<N;j++){
			fscanf(fp, "%d", &G[i][j]);
		}
	}
	fclose(fp);

	/*  w[N][N] : weight matrix
	    w = 1w[0]    1w[2]    ... 1w[N-1]
	    2w[0]    2w[2]    ... 2w[N-1]
	    :		  :
	    Nw[0]    Nw[1]    ... Nw[N-1]
	    const_num : the number of constraint
	*/
	int const_num = 0;
	double w[N][N];
		for(int i=0; i<N; i++) {	
		for(int j=0; j<N; j++){
			w[j][i] = ((double)j+1)*W[i];
		}
	}
	
	try
    {
		GRBEnv env = GRBEnv();
		GRBModel model = GRBModel(env);
		
		/* Set variables */
		GRBVar x[N][N];
		for(int i=0; i<N; i++) {
			for(int j=0; j<N; j++){
				x[i][j] = model.addVar(0.0, GRB_INFINITY, 0.0, GRB_BINARY);
			}
		}
 		model.update();

		/* Set the model to minimization */
		model.set(GRB_IntAttr_ModelSense, 1);
		GRBLinExpr Obj = 0;
		for(int i=0; i<N; i++){
			for(int j=0; j<N; j++){
				Obj += x[i][j]*w[i][j];
			}
		}
		model.setObjective(Obj);
				
		/* Set constraints */
		/* In each row the sum is 1 */
		for(int i=0; i<N; i++) {
			GRBLinExpr LHS=0;
			for(int j=0; j<N; j++) {
				LHS += x[i][j];
			}
			model.addConstr(LHS,GRB_EQUAL,1);
			const_num++;
		}
		/* In each column the sum is 1 */
		for(int i=0; i<N; i++) {
			GRBLinExpr LHS=0;
			for(int j=0; j<N; j++) {
				LHS += x[j][i];
			}
			model.addConstr(LHS,GRB_EQUAL,1);
			const_num++;
		}

		/* Precedence constraints */
		for(int k=0;k<N;k++){
			for(int l=k+1;l<N;l++){
				if(G[k][l]==1){
					for(int i=0; i<N; i++) {
						GRBLinExpr LHS=0;
						GRBLinExpr RHS=0;
						for(int j=i; j<N; j++) {
							LHS += x[j][k];
							RHS += x[j][l];
						}
					model.addConstr(LHS,GRB_LESS_EQUAL,RHS);
					const_num++;
					}
				}
			}
		}
		/* Run the optimizion */
                GRBEnv modelEnv = model.getEnv();
                modelEnv.set(GRB_DoubleParam_MIPGap, 0);
		clock_t end = clock();    // preprocessing time
		clock_t start2 = clock();     // optimization time
		model.optimize();
		clock_t end2 = clock();     // optimization time

		if(SHOW_PERM_PRE_TIME) std::cout << "Preprocessing time = " << (double)(end - start) / CLOCKS_PER_SEC << "sec.\n";
		if(SHOW_PERM_OPT_TIME) std::cout << "Optimization time = " << (double)(end2 - start2) / CLOCKS_PER_SEC << "sec.\n";
		Output[5] =(double)(end - start) / (double)CLOCKS_PER_SEC;
		Output[6] =(double)(end2 - start2) / (double)CLOCKS_PER_SEC;
		Output[14]=(double)const_num;
		
		for(int i=0; i<N; i++) {
			for(int j=0; j<N; j++) {
				if(x[j][i].get(GRB_DoubleAttr_X)==1.0){
					permutation_matrix_answer[i]=j+1;
					if(SHOW_PERM_ANS) printf("%d ",permutation_matrix_answer[i]); 
				}
			} 
		}
		if(SHOW_PERM_ANS) printf("\n");
		permutation_matrix_w = (double)model.get(GRB_DoubleAttr_ObjVal);
		if (SHOW_PERM_W) std::cout << "Obj: " << permutation_matrix_w << std::endl;


	}
    catch (GRBException e) 
    {
        std::cout << "Error code = "
                  << e.getErrorCode() 
                  << std::endl;
        std::cout << e.getMessage() << std::endl;
    }
    catch (...)
    {
        std::cout << "Exception during optimization"
                  << std::endl;
    }
	return 0;
}
