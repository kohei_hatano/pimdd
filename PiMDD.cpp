﻿#include <stdio.h>
#include <vector>
#include <set>
#include <queue>
#include <iostream>
#include <time.h>
#include <math.h>
#include <cfloat>
#include "main.h"

extern double Output[OUTPUT_SIZE];
extern double W[N];
extern int pimdd_answer[N];
extern double pimdd_w;
extern set<node_label> V;
extern vector<int> E;

/* 
	G[N][N] : DAG G
	Gout[N]
		Gout[j] : Node set with branches to node j in DAG G
	Vout[N] : outgoing degree of each node in the current DAG G
	pE : size of the current edge set
	pV : size of the current node set
	Edge : dynamic memory allocation for the edge set
*/
int G[N][N];
vector<int> Gout[N];
int pE;	
int pV;	
int Edge = EDGE;

int OutputPiMDD();
void MakeSubPiMDD(unsigned int *,int * ,int);
void Opt_main(vector<int> &);


unsigned int GetBits(unsigned int x,int p,int n){return ((x>>p)&~(~0x0000<<n));}
void ShowBits(unsigned int x){
  int nbit=sizeof(x);
  for(int i=1;i<=nbit*8;i++){
    printf("%x",GetBits(x,nbit*8-i,1));
    if(i%4==0) printf(" ");
  }
}
void SetBit(int p, unsigned int *x){*x=(*x&~((0x01)<<p));}
void CopySetBit(int p, unsigned int *x,unsigned int *y){*y=(*x&~((0x01)<<p));}

int PiMDD(){
	/* 
		<Program flow>
			1, Input DAG and calculate outgoing degree
			2, Initialization
			3, Construct PiMDD
			4, Optimazation
	*/
	clock_t start = clock();    // preprocessing time
	/* 1, Input DAG and calculate outgoing degree */
	for(int i=0;i<N;i++)	{
		Gout[i].reserve(N-i);
		Gout[i].erase(Gout[i].begin(), Gout[i].end());
	}
	FILE *fp;
	int Vout[N];
	if ((fp = fopen("dag.cvs", "r")) == NULL) {
		printf("dag file open error!!\n");
		return -1;
	}
	for(int tmpi,i=0;i<N;i++){
		tmpi=0;
		for(int j=0;j<N;j++){
			fscanf(fp, "%d", &G[i][j]);
			if(G[i][j]==1) {
				tmpi++;
				Gout[j].push_back(i);
			}
		}
		Vout[i]=tmpi;
	}
	fclose(fp);


	/* 2, Initialization */
	pE=0;
	pV=0;
	unsigned int v[N4];	// node label
	for(int i=0;i<N4;i++)	v[i]=~0;	// Calculate node label to pass first to MakeSubPiMDD
	
	
	/* 3,Construct PiMDD */
    MakeSubPiMDD(v,Vout,N);
	
	/* PiMDD standard output */
	if(SHOW_PiMDD){
		printf("\n");
		printf("\n");
		for(set<node_label>::iterator itr = V.begin(); itr != V.end(); ++itr){
			printf("%d  ",itr->Number);
			for(int j=0;j<N4;j++){
				ShowBits(itr->Label[j]);
			}
			printf("\n");
		}
		for(int i=0;i<pE;i++){
			printf("%d,%d,%d,%d",E[i*4],E[i*4+1],E[i*4+2],E[i*4+3]);
			printf("\n");	
		}
	}

	/* Output PiMDD */
	if(OUTPUT_PiMDD) OutputPiMDD();
		
	
	/* Preprocessing for optimization */
	/* 
		V2 : Information on the directed edge of each node in PiMDD
		V2[x*(N*3+1)] : The number of directed edge from node x
		V2[x*(N*3+1) + 3*j +1] : An end node of directed edge coming from node x
		V2[x*(N*3+1) + 3*j +2] : A difference node between the node label corresponding to the node number at the root of the edge and the node label corresponding to the node number at the end of the edge
		V2[x*(N*3+1) + 3*j +3] : size of node label corresponding to the node number at the root of the edge
	*/
	vector<int> V2( pV * (N*3+1) );
	for(int i=0;i<pV;i++){
		V2[i*(N*3+1)]=0;
	}

	for(int f,j,i=0;i<pE;i++){	//Make V2
		f = E[4*i];	//Which node think about
		j=V2[f*(N*3+1)];	
		V2[f*(N*3+1) + 3*j +1]=E[4*i+1];	
		V2[f*(N*3+1) + 3*j +2]=E[4*i+2];	
		V2[f*(N*3+1) + 3*j +3]=E[4*i+3];	
		V2[f*(N*3+1)]++;
	}

	/* Show V2 */
	if(SHOW_V2){
		for(int i=0;i<pV;i++){
			for(int j=0;j<V2[i*(N*3+1)];j++){
				printf("%d,%d,%d  " ,V2[i*(N*3+1) + 3*j+1] ,V2[i*(N*3+1) + 3*j+2] , V2[i*(N*3+1) + 3*j+3] );}
				printf("\n");
		}	
	}
	
	
	clock_t end = clock();     // preprocessing time
	/* 4, Optimazation */
	clock_t start2 = clock();    // optimization time
	Opt_main(V2);
	clock_t end2 = clock();     // optimization time
	if(SHOW_PiMDD_PRE_TIME) cout << "Preprocessing time = " << (double)(end - start) / CLOCKS_PER_SEC << "sec.\n";
	if(SHOW_PiMDD_OPT_TIME) cout << "Optimization time = " << (double)(end2 - start2) / CLOCKS_PER_SEC << "sec.\n";
	Output[2] =(double)(end - start) / CLOCKS_PER_SEC;
	Output[3] =(double)(end2 - start2) / CLOCKS_PER_SEC;
	Output[11] = (double)pV;
	Output[12] = (double)pE;
	return 0;
}


void MakeSubPiMDD(unsigned int *v,int *Vout,int depth){
	/*  
		V : node set
		E : edge set
		v[] : Given node label
		Vout : Given outgoing degree of each node
		Vout_ : Current outgoing degree of each node
		pV : Size of the current node set
		pE : Size of the current edge set
		depth : The size of given node label 
		tempv[] : Temporarily hold node label
		from_node : Root node of the edge
		to_node : End node of the edge
		qu : Node set with outgoing degree 0

		<Program flow>
		1,Determine whether more dynamic securing is necessary for E
		2,Add given node label to node set
		3,Calculate node set with outgoing degree 0
		4,For each outgoing degree 0 node
		5a,Connect an edge to an existing node
		5b,Connect an edge to a new node label
	*/
	int i,j;
	unsigned int tmpv[N4];
	int from_node = pV;
	int to_node;	
	queue<int> qu;
	set<node_label>::iterator sitr;

	/* 1,Determine whether more dynamic securing is necessary for E */
	if(pE>=Edge-(2*N)){
		Edge = 2 * Edge;
		if(SHOW_EDGE_UPDATE) cout << "Edge=" << Edge << endl;
		E.resize( 4*Edge );
		}
	
	/* 2,Add given node label to node set */
	V.insert(node_label(pV++,v));


	/* 3,Calculate node set with outgoing degree 0 */
	for (i=0;i<N;i++){
		if(Vout[i]==0&&GetBits(v[i/32],31-(i%32),1)){
			qu.push(i);
		}
	}


	/* 4,For each outgoing degree 0 node */
	while(!qu.empty()){
		i=qu.front();
		qu.pop();
		int Vout_[N];
		for (j=0;j<N;j++){
			Vout_[j]=Vout[j];
		}
		
		
		/* Calculate tmpv[] which removed node i from v[] */
		for(j=0;j<N4;j++) tmpv[j]=v[j];
		CopySetBit(31-(i%32),&v[i/32],&tmpv[i/32]);


		/* Whether node label tempv[] has already exists in V */
		sitr = V.find(node_label(0,tmpv));


		/* Calculate edge */
		if(sitr!= V.end()){	
			/* 5a,Connect an edge to an existing node */
			to_node = sitr->Number;
		}
		else{
			/* 5b,Connect an edge to a new node label */
			/* Update outgoing degree when node i is excluded */
			for(vector<int>::iterator itr=Gout[i].begin();itr != Gout[i].end(); ++itr){
				Vout_[*itr]--;
			}
			to_node = pV;
			MakeSubPiMDD(tmpv,Vout_,depth-1);
		}
		E[pE*4]=from_node;
		E[pE*4+1]=to_node;
		E[pE*4+2]=i;
		E[pE*4+3]=depth;	
		pE++;
	}

}

int OutputPiMDD(){
	FILE *fp;
	fp = fopen("pimdd.dot","w");
	if (fp == NULL){
		printf("%s can not find ¥n", "pimdd.dot");
		return -1;
	}

	fprintf(fp,"digraph sample {\n");
	for(int i=0;i<pV;i++){
		fprintf(fp,"%d; \n",i);
	}
	for(int i=0;i<pE;i++){
		fprintf(fp,"%d -> %d [label = \"%d,%d\" ] ;\n",E[i*4],E[i*4+1],E[i*4+2],E[i*4+3]);
	}
	fprintf(fp,"}");
	fclose(fp);
	return 0;
}


void Opt_main(vector<int> &V2){
	/*
		U : Dynamic allocation of information held by each node. i.e.) U[0]: Weight held by node 0 U[1]: Weight held by node 1
		U2 : Dynamic allocation of information held by each node. i.e.) U2[0]: Node pointed by node 0 U2[1]: Node indicated by node 1
	*/
	vector<double> U(pV);
	vector<int> U2(pV);
	/* Initialization */
	for(int i=0;i<pV;i++){
		U[i] = DBL_MAX;	// Maximize the weights of all nodes
		U2[i] = -1;	// Not all nodes point to anything
	}
	U[0]=0; // The weight of the root node is 0
	queue<int> qu; 
	qu.push(0); // Follow from root node
	vector<unsigned int> qubit((pV/32)+1);//Dynamic allocation to manage which node was processed
	for(int i=0;i<((pV/32)+1);i++){
		qubit[i]=0;
	}
	qubit[((pV/32)+1)-1]=~0;
	for(int i=0;i<(pV%32);i++){
		SetBit(31-i,&qubit[((pV/32)+1)-1]);
	}
	int t;
	
	do{
		t = qu.front();
		qu.pop();
		for(int k,i=0;i<V2[t*(3*N+1)];i++){	//Loop by the number of edge from node t
			k=t*(N*3+1) + 3*i +1;	
			if(U[V2[k]]>(U[t]+((double)V2[k+2]*W[V2[k+1]]))){	
				U[V2[k]] = (U[t]+((double)V2[k+2]*W[V2[k+1]]));
				U2[V2[k]] = t;
				if(!GetBits(qubit[t/32],31-(t%32),1)){	
					qu.push(V2[k]);
					SetBit(31-(t%32),&qubit[t/32]);
				}
					
			}
		}
	}while((!qu.empty()));
		

	if(SHOW_MINPATH) cout << "The shortest path is ["<<endl;
	int pi[N+1];
	pi[N] = N;//Leaf node
	for(int i=N;i>0;i--){	//Follow shortest path from leaf node
		if(SHOW_MINPATH) printf("%d ",pi[i]);
		pi[i-1] =U2[pi[i]];
	}
	if(SHOW_MINPATH)	printf("%d]\n",pi[0]);
	if(SHOW_PiMDD_W) cout << "Obj: " << U[N] <<  endl;
	pimdd_w = U[N];

	int pimdd_temp_answer[N]; //Inverse permutation
	for(int j,i=0;i<N;i++){
		j=0;
		do{
			if(E[4*j]==pi[i]&&E[4*j+1]==pi[i+1]){
				pimdd_temp_answer[N-1-i] = E[4*j+2];
				break;
			}
			j++;
		}while(1);
	}

	/* Calculate permutation */
	for(int i=0;i<N;i++){
		pimdd_answer[pimdd_temp_answer[i]]=i+1;
	}

	if(SHOW_PiMDD_ANS){
		for(int i=0;i<N;i++){
			printf("%d ",pimdd_answer[i]);
		}
		printf("\n");
	}

}
