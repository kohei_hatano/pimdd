#include <stdio.h> 
#include "main.h"
#include "gurobi_c++.h"
#include <time.h>

extern double Output[OUTPUT_SIZE];
extern double W[N];
extern int comparison_matrix_answer[N];
extern double comparison_matrix_w;


void ComparisonMatrix(){
	clock_t start = clock();    // preprocessing time
	/* Input DAG */
	FILE *fp;
	int G[N][N];
	if ((fp = fopen("dag.cvs", "r")) == NULL) {
		printf("yomikomi file open error!!\n");
	}
	for(int i=0;i<N;i++){
		for(int j=0;j<N;j++){
			fscanf(fp, "%d", &G[i][j]);
		}
	}
	fclose(fp);

	/*  w[N][N] : weight matrix
	    w = W[0] W[1] ...  W[N]	
	    W[0]   W[1] ...  W[N]	
	     :	    :         :				
	    W[0]	     W[N]

		const_num : the number of constraint
	*/
	int const_num = 0;
	double w[N][N]={};
	for(int i=0; i<N; i++) {	
		for(int j=0; j<N; j++){
			w[j][i] = W[i];
		}
	}

	try
    {
		GRBEnv env1 = GRBEnv();
		GRBModel model1 = GRBModel(env1);
		
		/* Set variables */
		GRBVar x[N][N];	
		for(int i=0; i<N; i++) {
			for(int j=0; j<N; j++) {
				x[i][j] = model1.addVar(0.0, GRB_INFINITY, 0.0, GRB_BINARY);
			}
		}
		model1.update();

		/* Set the model1 to minimization */
		model1.set(GRB_IntAttr_ModelSense, 1);
		GRBLinExpr Obj = 0;
		for(int i=0; i<N; i++){
			for(int j=0; j<N; j++){
				Obj += x[j][i]*w[j][i];
			}
		}
		model1.setObjective(Obj);
		
		/* Set constraints */
		/* Symmetry constraints */
		for(int i=0;i<N;i++){
			for(int j=i+1;j<N;j++){
				model1.addConstr(x[i][j],GRB_EQUAL, 1 - x[j][i]);
				const_num++;
			}
		}

		/* Triangular inequality constraints */
		for(int i=0;i<N;i++){
			for(int j=0;j<N;j++){
				for(int k=0;k<N;k++){
					model1.addConstr(1,GRB_GREATER_EQUAL,x[i][j] - x[i][k] + x[j][k]);
					model1.addConstr(x[i][j] - x[i][k] + x[j][k],GRB_GREATER_EQUAL,0);
					const_num +=2;
				}
			}
		}

		/* Precedence constraints */
		for(int k=0;k<N;k++){
			for(int l=k+1;l<N;l++){
				if(G[k][l]==1){
					model1.addConstr(x[k][l],GRB_EQUAL,1);
					const_num++;
				}
			}
		}

		for(int i=0;i<N;i++){
		  model1.addConstr(x[i][i],GRB_EQUAL,1);
		  const_num++;
		}


      		/* Run the optimizion */
		GRBEnv modelEnv = model1.getEnv();
		modelEnv.set(GRB_DoubleParam_MIPGap, 0);
		clock_t end = clock();     // preprocessing time
		clock_t start2 = clock();     // optimization time
		model1.optimize();
		clock_t end2 = clock();     // optimization time

		if(SHOW_COMP_PRE_TIME) std::cout << "Preprocessing time = " << (double)(end - start) / CLOCKS_PER_SEC << "sec.\n";
		if(SHOW_COMP_OPT_TIME) std::cout << "Optimization time = " << (double)(end2 - start2) / CLOCKS_PER_SEC << "sec.\n";
		Output[8] =(double)(end - start) / (double)CLOCKS_PER_SEC;
		Output[9] =(double)(end2 - start2) / (double)CLOCKS_PER_SEC;
		Output[10] = Output[8] + Output[9];
		Output[13] = const_num;
		
		for(int i=0; i<N; i++) {
			comparison_matrix_answer[i] = 0;
			for(int j=0; j<N; j++) {
					comparison_matrix_answer[i] += (int)x[j][i].get(GRB_DoubleAttr_X); 
			}
			if(SHOW_COMP_ANS) printf("%d ",comparison_matrix_answer[i]);
		}
		if(SHOW_COMP_ANS) printf("\n");
		comparison_matrix_w = (double)model1.get(GRB_DoubleAttr_ObjVal);
		if (SHOW_COMP_W) std::cout << " Obj: "<< comparison_matrix_w << std::endl;
		
	}
    catch (GRBException e) 
    {
        std::cout << "Error code = "
                  << e.getErrorCode() 
                  << std::endl;
        std::cout << e.getMessage() << std::endl;
    }
    catch (...)
    {
        std::cout << "Exception during optimization"
                  << std::endl;
    }
}
