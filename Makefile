pimdd: main.o PiMDD.o DAG.o ComparisonMatrix.o PermutationMatrix.o
	g++ -o pimdd main.o PiMDD.o DAG.o ComparisonMatrix.o PermutationMatrix.o -L/usr/local/gurobi650/linux64/lib -lgurobi_c++ -lgurobi65
main.o: main.cpp
	g++ -Wall -c main.cpp
PiMDD.o: PiMDD.cpp
	g++ -Wall -c PiMDD.cpp
DAG.o: DAG.cpp
	g++ -Wall -c DAG.cpp
ComparisonMatrix.o: ComparisonMatrix.cpp
	g++ -I /usr/local/gurobi650/linux64/include -Wall -c ComparisonMatrix.cpp
PermutationMatrix.o: PermutationMatrix.cpp
	g++ -I /usr/local/gurobi650/linux64/include -Wall -c PermutationMatrix.cpp
clean:
	rm -f *.o pimdd